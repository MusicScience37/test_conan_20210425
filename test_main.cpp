#include <iostream>

#include "test_messages.pb.h"

int main() {
    test_messages::StringData data;
    data.set_data("test");
    const auto serialized_data = data.SerializeAsString();
    data.ParseFromString(serialized_data);
    if (data.data() == "test") {
        std::cout << "success" << std::endl;
    } else {
        std::cout << "fail" << std::endl;
    }
    return 0;
}
