from conans import ConanFile, CMake


class TestConan(ConanFile):
    requires = "protobuf/3.15.5"
    generators = "cmake_find_package"
